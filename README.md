# 몽고DB + Spring Framework Major Version Up

#### #개요
다음과 같이 MongoDB 메이저 버전업과 그에 따른 spring-data, springframework 버전업 과정에 어플리케이션 코드 변경 사항 기록.

||기존|이행|
|---|---|---|
|MongoDB|3.4|4.2|
|mongo-java-driver|3.10.2|3.12.2|
|spring-data-mongo|1.10.22.RELEASE|2.2.6.RELEASE|  
|spring-data-jpa|1.10.22.RELEASE|2.2.6.RELEASE|  
|springframework|4.3.25.RELEASE|5.2.3.RELEASE|  


#### #종합  
몽고DB클라이언트 업데이트에 따른 API 변경 대응.  
코드 변경은 다음과 같음.  


#### #목록 조회 `PageRequest` , `Sort` 생성 방식 변경.  
![](./assets/image2020-4-21_19-42-16.png)


#### #`com.mongodb.WriteResult` 가 사라지고 각 오퍼레이션 결과가  `xxxResult`로 세분화 대체 되어 `.getN()` 을 `.getModifiedCount()` 로 변경.
![](./assets/image2020-4-21_19-42-49.png)


#### #다건 조작 API  `.xxxx(List<>)` 가 `.xxxxAll(List<>)` 로 변경.  
![](./assets/image2020-4-21_19-43-13.png)


#### #단 건 데이터 조회하는 곳에  `findById()` 혹은 `findOne()` 을  `getOne()`으로 변경시 유의 사항.  
> findById() 혹은 findOne()의 경우 데이터 존재하지 않으면 null을 리턴하지만 getOne()은 데이터 사용시점에 EntityNotFoundException 발생.  
> DB에 존재하지 않는 id 조회하는 경우 동작에 차이 있음. 

![](./assets/image2020-4-21_19-43-32.png)


#### #`DBObject` to `Document` 
![](./assets/image2020-4-21_19-43-53.png)


#### #`Log4jConfigListener` 사라짐 (Spring Framework)
![](./assets/image2020-4-21_19-44-12.png)


#### #Java 측 `@Document` 엔티티의 `@Indexed`, `@CompoundIndexes` 지정한 인덱스 이름이 서버에 생성된 이름과 일치하지 않는 경우 기존은 warn 뱉고 진행하지만 이제는 예외를 뱉고 중단
인덱스 자동 생성 무효화.  
![](./assets/image2020-4-21_19-49-37.png)


#### #Query with `PageRequest` (spring-data)
> 아래와 같은 count 질의가 있다면 기존 버전에서는 pagenation 조건을 배재한 totalCount를 응답하지만 
> 이행 버전에서는 pagenation을 적용한 totalCount를 응답

~~~java
Query.query(where).with(pageRequest)
mongoOperations.count(query, ReplyMongo.class);
~~~


#### #`AbstractMongoEventListener` 를 @Bean 으로 등록 (Spring Framework)
![](./assets/image2020-4-21_19-56-36.png)


#### #`.findById()` 계열이 `Optional<T>` 응답으로 변경 (spring-data) 
![](./assets/image2020-4-21_20-1-13.png)


#### #`Pageable` 변경.
![](./assets/image2020-5-14_18-29-56.png)


#### #Spring `ResponseEntity.getStatusCode()` 응답 형식 변경.
> 기존 "200"  현행 "200 OK"
![](./assets/image2020-4-28_22-9-28.png)


#### #Spring Data JPA  `findByXXXX()` 메소드 지원. 
> 기존 단일 객체로 응답을 받으면 단일 row 응답하나  
> 현행은 쿼리 결과가 멀티라면 예외 발생.  



